## Work Experience at GitLab (2023-07-27 to 2023-07-28)

### 2023-07-27

1. Looked at schedule for the day.
1. Set up a [GitLab profile](https://gitlab.com/FreddieRichards).
1. Created a [project on GitLab](https://gitlab.com/freddie3625530/Freddie).
1. Help write 1:1 notes for meetings today with a distinguished engineer, a senior director and four managers. Including introducing myself and asking:
   1. What is your job?
   1. How did you get into your job?
      1. (Distinguished engineer) Dad had a computer and could only go on if he coded. Coded his own games.
      1. (Manager1) Dad made software for trains and had a computer which he learned to code on that.
      1. (Senior director) Liked to fix certain things such as TVs. Break stuff and then fix them.
      1. (Manager2) Got a degree in software engineering and worked on games. Then after started working on GitLab.
      1. (Manager3) Got a degree and went into some support jobs then infrastructure jobs and finnaly into manager jobs.
      1. (Manager4) Had a physics degree and liked maths and got more into the logic area of computing.
   1. What advice do you have for getting into software engineering?
      1. (Distinguished engineer) Experiment and make something that interests you. 
      1. (Manager1) Try stuff you like and use real world examples.
      1. (Senior director) Try very hard at many different things a lot. You can change the things you like.
      1. (Manager2) Have an interest and build on the interest. Try and you are the only person who can try for yourself.
      1. (Manager3) Find things your interested in and pursue them.
      1. (Manager4) Go to different open source software and learn how to have first hand expirience.
1. Reviewed a purchase order for cloud services for over 1 million USD.  
1. Created a new file for this diary.
1. Found a issue on the [GitLab Infrastructure handbook](https://about.gitlab.com/handbook/engineering/infrastructure/#infrastructure-office-hours).
   1. Firstly I had to fork the problem from https://gitlab.com/gitlab-com/www-gitlab-com/-/forks to https://gitlab.com/freddie3625530/www-gitlab-com.
   1. Secondly I couldn't find where to fix my problem in the forked project. 
   1. Then I found the location by scrolling through the folders until I found the `sites` folder. From there I looked at the web adress and navigated through the folders before I found the same title and modified `index.html.rb` because its the default file. Then I changed it and submitted a [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/127542) to have the change be implemented.
   1. For the description in the [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/127542) I first described the what of the problem but changed it to the why. 
   1. I then had to tell the bot that it was ready then it got assigned to a reviewer, then I had to wait for their response.
1. Found another issue on the [GitLab Infrastructure handbook](https://about.gitlab.com/handbook/engineering/infrastructure/#stable-counterparts).
   1. This issue was with the link not working.
   1. This time I already knew where to find the code to change and could change it quickly.
   1. Once again with my [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/127551) I did the what but had to look back and change it to add the why, even though it was obvious.
   
### 2023-07-28

1. Checked my todo list and there was about 10 messages from my merge requests. Thanking me for my contribution and requesting feedback.
1. I nominated myself for a swag prize as my first merge request got accepted.
1. Downloaded and set up vs code because gitlab web ide dosn't run code.
   1. Installed python extension and python.
   1. Tryed to install git, but needed homebrew.
   1. Installed homebrew, then git.
1. I did problem number 1 and 2 of [Project Euler](https://projecteuler.net/archives) and attempted 3 and 5 but didnt finish.
1. After that I did some python challenges on [learnpython.org](https://www.learnpython.org/en/Variables_and_Types). But it was too easy for me so I stopped and write the last part of the diary for today. 