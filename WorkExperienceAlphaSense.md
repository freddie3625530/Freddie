## Work Experience at [AlphaSense](http://alpha-sense.com) (2024-10-21 to 2024-10-25)

### 2024-10-21

1. I already had been given a computer by the company however I had to get it set up. I set up a few apps such as a vpn and made sure everything else worked (emails, slack, calendar)
1. Setup vs code to clone the developer portal and work on it. 
1. I did this until the person helping me, Amy, for the week came.
1. She helped me finish seting up being able to access and make changes to the developer portal.
1. Next I attended the API teams daily meeting at 10:30 everyday except Wednesday. Where the team talks about what they did yesterday or Friday as it was Monday and what they would do today. I listened to this as I had no plans and had done no work yet.
1. After I went to a meeting with the manager of the API team, Liudmila. Where she talked about the values of the company, what my goals for this internship is and what the API team does.
1. Then after lunch I started actual work as Amy assigned me to my first ticket for alpha sense. For this ticket I had to see the developer portal pages and see which ones were missing a table of contents.
    1. Firstly I looked through all the developer portal pages, noting down which ones had no table of contents, three pages didn't have one.
    1. Secondly I opened my cloned version of developer portal in vs code as I had already set that up.
    1. Next I found the pages within this and Amy told me the easiest way to fix this issue is to convert the file to an md file where it would be auto generated.
    1. So then I converted my first file 'document sharing' from .mdx file type to .md file
    1. Then I converted all the .mdx formating into correct syntax for an md file.

### 2024-10-22

1. I resumed my original ticket, starting and completing the second page in the same way as the first.
1. Then for the third page I was about to start it then I realised there was no headings, therefore no contents needed. So I was able to close the ticket.
    1. Firstly I made sure I was in a different branch of the main page.
    1. Then I created my own version using 
        1. git add .
        1. git commit "chore: added table of contents"
        1. git push
    1. This meant that I had created a pull request for my changes which needed to be looked at by some people in order for the changes to be made.
1. Then I attended the daily meeting with the team, this time sharing my progress as well as listening to everyone elses progress. I mentioned that I had just finished one ticket so someone else from the team assigned me another one.
1. This one was on devhub and was to make some pages under the 'graphql' heading have coloured notices such as 'tips' 'important' and 'info' boxes to display information.
    1. Firstly I had to clone devhub onto my vs code.
    1. Then I read the README file to see how to make changes to the page.
    1. I followed the README instructions and then found my way to the pages which needed the coloured notices.
    1. I didn't know how to make them coloured so I found another page which used the colours.
    1. I applied the same synstax to replicate the design on that page to the ones I needed to.

 ### 2024-10-23

 1. I started the day by using the same steps as before to create a pull request for this ticket.
 1. Then I noticed I had another ticket assigned to me, this time still in devhub, however this one was to migrate a page into devhub from a different source.
    1. Firstly I copy and pasted the text from the website (in chunks) into my branch of devhub in vs code, making sure to make the text links go to the correct place and make the syntax .md friendly
    1. Next I had the person assigning the ticket to me look over my work and he said that some of the information could be removed as it was outdated.
    1. So I removed this information and made a pull request for it. However I messed this up slightly and commited the new changes in the ticket I had started the day before, however Amy helped my fix this and I created two seperate pull requests for my two seperate tickets.

### 2024-10-24

1. I started the day with the daily meeting where I once again said what I have been doing and listened to what everyone else had been doing.
1. I then looked over my previous pull requests and saw a few things
    1. My first pull request had now been allowed to be tested which I did and moved it to the next stage of testing.
    1. My second ticket had been accepted by two people with very good feedback.
    1. My third ticket had been looked at and accepeted by one person.
1. As the latter two where on devhub I couldn't access it and the changes had to be performed by someone else so they were done in terms of what I needed to do.
1. However I still needed to deploy my first ticket to RC testing where myself and a manager had to look over the changes, in this case it was Liudmila.
1. My changes were accepted and therefore I could deploy them to PROD which meant they were actually on the developer portal website for alpha sense.
1. I also started two tickets.
    1. The first one I had to remove the unused 'keys' section in the sidebar.
    1. To do this I went back to developer portal and made a new branch.
    1. I then just deleted the line where it said how keys was used.
    1. Finally I did all the step needed for a pull request, however this one wouldn't have enough time for me to go throught all the testing phases so it was sent to somone else to do that for me.

### 2024-10-25

1. I also started the second ticket which was to fix an issue on how in safari on Iphone the privacy policy in the footer would be paritally cut off.
    1. Firstly I found the footer part and tried to find out how to fix the issue, however I couldn't be sure if they were working.
    1. As I hadn't used safari before much and had to find out how to replicate a mobile device on my laptop to ensure my changes had worked. Which took a long time.
1. However by the time I had figured out how to do this it was time for the daily meeting where I said what I had done over the week and said thank you and goodbye to the team.